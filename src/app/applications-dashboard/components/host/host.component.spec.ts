import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { HostComponent } from './host.component';
import { Application } from '../../models/application.model';
import { DebugElement } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

/* NgRx */
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import * as applicationsReducer from '../../store/reducers/applications.reducer';
import { ApplicationsEffect } from '../../store/effects/applications.effect';


describe('HostComponent', () => {
  let component: HostComponent;
  let fixture: ComponentFixture<HostComponent>;
  const hostNameMock = 'hostName';
  const applicationsMock = [
    {
      name: 'App6',
      apdex: 6,
      contributors: ['contributor1', 'contributor2'],
      host: ['host1', 'host2'],
      version: 7
    },
    {
      name: 'App5',
      apdex: 5,
      contributors: ['contributor1', 'contributor2'],
      host: ['host1', 'host2'],
      version: 7
    },
    {
      name: 'App4',
      apdex: 4,
      contributors: ['contributor1', 'contributor2'],
      host: ['host1', 'host2'],
      version: 7
    },
    {
      name: 'App3',
      apdex: 3,
      contributors: ['contributor1', 'contributor2'],
      host: ['host1', 'host2'],
      version: 7
    },
    {
      name: 'App2',
      apdex: 2,
      contributors: ['contributor1', 'contributor2'],
      host: ['host1', 'host2'],
      version: 7
    },
    {
      name: 'App1',
      apdex: 1,
      contributors: ['contributor1', 'contributor2'],
      host: ['host1', 'host2'],
      version: 7
    },
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HostComponent ],
      imports: [
        HttpClientModule,
        StoreModule.forRoot({}),
        StoreModule.forFeature(applicationsReducer.applicationsFeatureKey, applicationsReducer.applicationsReducer),
        EffectsModule.forRoot([]),
        EffectsModule.forFeature([ApplicationsEffect])
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('host component test', () => {
    beforeEach(() => {
      component.applications = applicationsMock;
      component.hostName = hostNameMock;
    });

    it('should retrieve the top five applications by apdex', () => {
      const topFive: Application[] = component.getTopFiveApps();
      expect(topFive.length === 5).toBeTruthy();

      let index = 6;
      topFive.forEach((app: Application) => {
        expect(app.name).toEqual(`App${index}`);
        index--;
      });
    });

    it('should render the host name inside the host card', () => {
      const hostNameEl: HTMLElement = fixture.debugElement.query(
        By.css('.host-name span')
        ).nativeElement;

      fixture.detectChanges();
      expect(hostNameEl.textContent).toEqual('hostName');
    });

    it('should render host name and the top five applications inside the host card', () => {
      const cardContentDe: DebugElement = fixture.debugElement.query(By.css('.card-content'));
      const cardContentEl: HTMLElement = cardContentDe.nativeElement;
      fixture.detectChanges();
      const expectedCardTextContent = 'hostName 6 App6 5 App5 4 App4 3 App3 2 App2';
      expect(cardContentEl.textContent).toEqual(expectedCardTextContent);
    });
  });
});
