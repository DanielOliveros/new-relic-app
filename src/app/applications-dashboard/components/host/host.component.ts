import { Component, OnInit, Input } from '@angular/core';
import { Application } from '../../models/application.model';
import { Store } from '@ngrx/store';
import * as ApplicationsActions from '../../store/actions/applications.action';

@Component({
  selector: 'app-host',
  templateUrl: './host.component.html',
  styleUrls: ['./host.component.scss']
})
export class HostComponent implements OnInit {
  @Input() hostName: string;
  @Input() applications: Application[];
  @Input() showAsList: boolean;
  constructor(private store: Store<any>) { }

  ngOnInit() {
  }

  getTopFiveApps() {
    return this.applications ? this.applications.slice(0, 5) : [];
  }

  onAppClicked(application: Application) {
    const alertText = `Application clicked: ${application.name} (Release version: ${application.version})`;
    window.alert(alertText);
  }

  onRemoveApp(application: Application) {
    this.store.dispatch(new ApplicationsActions.RemoveApplication({ appToRemove: application }));
  }

}
