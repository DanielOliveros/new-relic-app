import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/* NgRx */
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import * as applicationsReducer from '../../store/reducers/applications.reducer';
import { ApplicationsEffect } from '../../store/effects/applications.effect';

import { ApplicationsDashboardComponent } from './applications-dashboard.component';
import { HostComponent } from '../host/host.component';

describe('ApplicationsDashboardComponent', () => {
  let component: ApplicationsDashboardComponent;
  let fixture: ComponentFixture<ApplicationsDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        StoreModule.forRoot({}),
        StoreModule.forFeature(applicationsReducer.applicationsFeatureKey, applicationsReducer.applicationsReducer),
        EffectsModule.forRoot([]),
        EffectsModule.forFeature([ApplicationsEffect])
      ],
      declarations: [ ApplicationsDashboardComponent, HostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationsDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
