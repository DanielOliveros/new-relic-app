import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { FormGroup, FormControl } from '@angular/forms';
import { Application } from '../../models/application.model';

import * as ApplicationsActions from '../../store/actions/applications.action';

@Component({
  selector: 'app-applications-dashboard',
  templateUrl: './applications-dashboard.component.html',
  styleUrls: ['./applications-dashboard.component.scss']
})
export class ApplicationsDashboardComponent implements OnInit, OnDestroy {
  addAppForm = new FormGroup({
    appName: new FormControl(''),
    appApdex: new FormControl(''),
    appHostName: new FormControl(''),
  });
  public hostListMap: Map<string, Application[]>;
  private subscription;
  private listMode: boolean;
  private userEmail = 'averylongemailaddress@companyname.com';
  constructor(private store: Store<any>) { }

  ngOnInit() {
    this.store.dispatch(new ApplicationsActions.LoadApplications());
    this.subscription = this.store.select('applicationsDashboard').subscribe(applicationsState => {
      this.hostListMap = applicationsState.hostListMap;
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getHosts() {
    return Array.from(this.hostListMap.entries());
  }

  showList(checked: boolean) {
    this.listMode = checked;
  }

  getTopAppsByHost(hostName: string) {
    return this.hostListMap.get(hostName) ? this.hostListMap.get(hostName).slice(0, 25) : [];
  }

  onSubmit() {
    if (this.addAppForm.valid) {
      const newApp: Application = {
        name: this.addAppForm.controls.appName.value,
        apdex: this.addAppForm.controls.appApdex.value,
        host: [this.addAppForm.controls.appHostName.value],
        contributors: ['contributor 1'],
        version: 8
      };
      this.store.dispatch(new ApplicationsActions.AddApplication({ newApp }));
    }
  }

}
