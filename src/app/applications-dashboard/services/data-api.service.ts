import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';

import { Application } from '../models/application.model';

@Injectable({
  providedIn: 'root'
})
export class DataApiService {
  applicationsDataUrl = 'assets/host-app-data.json';

  constructor(private http: HttpClient) { }

  getAll(): Observable<Application[]> {
    return this.http.get<Application[]>(this.applicationsDataUrl);
  }
}
