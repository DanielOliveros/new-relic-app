import { TestBed, inject } from '@angular/core/testing';

import { DataApiService } from './data-api.service';
import { HttpClientModule } from '@angular/common/http';

import { Application } from '../models/application.model';

describe('DataApiService', () => {

  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule,
    ],
    providers: [
      DataApiService
    ]
  }));

  it('should be created', () => {
    const service: DataApiService = TestBed.get(DataApiService);
    expect(service).toBeTruthy();
  });

  it('should return an Observable<Application[]>', () => {
    inject([DataApiService], (DataApiService) => {
      DataApiService.getAll().subscribe((applications: Application[]) => {
        expect(Array.isArray(applications)).toBe(true);
      });
    });
  });
});
