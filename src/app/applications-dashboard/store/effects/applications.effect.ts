import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs';
import { EMPTY } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { DataApiService } from '../../services/data-api.service';
import { Application } from '../../models/application.model';

@Injectable()
export class ApplicationsEffect {

    @Effect()
    loadApplications$: Observable<Action> = this.actions$
        .pipe(
            ofType('[ApplicationsDashboard Component] Load Applications'),
            mergeMap(() => this.dataApiService.getAll()
                .pipe(
                    map((applications: Application[]) => ({
                        type: '[Data API] LoadApplications Success',
                        payload: {applications}
                    })),
                    catchError(() => EMPTY)
                )
            )
        );

  constructor(
    private actions$: Actions,
    private dataApiService: DataApiService
  ) {}
}
