import * as ApplicationsActions from '../actions/applications.action';
import { Application } from '../../models/application.model';

export interface State {
    hostListMap: Map<string, Application[]>;
}

export const initialState: State = {
    hostListMap: new Map<string, Application[]>()
};

export const applicationsFeatureKey = 'applicationsDashboard';

export function applicationsReducer(state = initialState, action: ApplicationsActions.ActionsUnion): State {
    switch (action.type) {

        case ApplicationsActions.ActionTypes.LoadApplicationsSuccess: {
            const newState: State = {
                hostListMap: new Map<string, Application[]>()
            };
            action.payload.applications.sort((a, b) => b.apdex - a.apdex);
            action.payload.applications.forEach((application: Application) => {
                application.host.forEach((host: string) => {
                    if (newState.hostListMap.has(host)) {
                        newState.hostListMap.get(host).push(application);
                    } else {
                        newState.hostListMap.set(host, [application]);
                    }
                });
            });
            return { ...newState };
        }

        case ApplicationsActions.ActionTypes.AddApplication: {
            const newApp: Application = action.payload.newApp;
            const newState = { ...state };
            newApp.host.forEach((host: string) => {
                newState.hostListMap.get(host).push(newApp);
                newState.hostListMap.get(host).sort((a, b) => b.apdex - a.apdex);
            });
            return { ...newState };
        }

        case ApplicationsActions.ActionTypes.RemoveApplication: {
            const appToRemove: Application = action.payload.appToRemove;
            const newState = { ...state };
            let auxAppsList: Application[];
            appToRemove.host.forEach((host: string) => {
                auxAppsList = newState.hostListMap.get(host)
                .filter((application: Application) => application.name !== appToRemove.name);
                newState.hostListMap.set(host, [...auxAppsList]);
            });
        }
        default: {
            return state;
        }
  }
}
