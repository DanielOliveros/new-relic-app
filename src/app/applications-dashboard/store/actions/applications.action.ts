import { Action } from '@ngrx/store';
import { Application } from '../../models/application.model';

export enum ActionTypes {
  LoadApplications = '[ApplicationsDashboard Component] Load Applications',
  LoadApplicationsSuccess = '[Data API] LoadApplications Success',
  LoadApplicationsError = '[Data API] LoadApplications Error',
  AddApplication = '[ApplicationsDashboard Component] Add Application',
  RemoveApplication = '[ApplicationsDashboard Component] Remove Application'
}

export class LoadApplications implements Action {
  readonly type = ActionTypes.LoadApplications;
}

export class LoadApplicationsSuccess implements Action {
  readonly type = ActionTypes.LoadApplicationsSuccess;
  constructor(public payload: { applications: Application[]}) {}
}

export class LoadApplicationsError implements Action {
  readonly type = ActionTypes.LoadApplicationsError;
}

export class AddApplication implements Action {
  readonly type = ActionTypes.AddApplication;
  constructor(public payload: { newApp: Application}) {}
}

export class RemoveApplication implements Action {
  readonly type = ActionTypes.RemoveApplication;
  constructor(public payload: { appToRemove: Application }) {}
}

export type ActionsUnion = LoadApplications |
LoadApplicationsSuccess |
LoadApplicationsError |
RemoveApplication |
AddApplication;
