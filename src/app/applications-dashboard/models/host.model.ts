import { Application } from './application.model';
export class Host {
    name: string;
    applications: Application[];
}
