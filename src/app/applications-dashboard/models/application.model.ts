export interface Application {
    name: string;
    contributors: string[];
    version: number;
    host: string[];
    apdex: number;
}
