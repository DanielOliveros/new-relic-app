import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/* NgRx */
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import * as applicationsReducer from './store/reducers/applications.reducer';
import { ApplicationsEffect } from './store/effects/applications.effect';

import { ApplicationsDashboardComponent } from './components/applications-dashboard/applications-dashboard.component';
import { HostComponent } from './components/host/host.component';

const customerRoutes: Routes = [{ path: '', component: ApplicationsDashboardComponent }];

@NgModule({
  declarations: [
    ApplicationsDashboardComponent,
    HostComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(customerRoutes),
    StoreModule.forFeature(applicationsReducer.applicationsFeatureKey, applicationsReducer.applicationsReducer),
    EffectsModule.forFeature([ApplicationsEffect])
  ],
  exports: [
    ApplicationsDashboardComponent,
    HostComponent
  ]
})
export class ApplicationsDashboardModule { }
