import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {
  ApplicationsDashboardComponent
} from './applications-dashboard/components/applications-dashboard/applications-dashboard.component';

const routes: Routes = [
  {
    path: 'applications',
    component: ApplicationsDashboardComponent
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'applications'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
