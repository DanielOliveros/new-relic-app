# NewRelicApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.8 as part of an application process to New Relic. This README document provides information about the technologies used, the reason why these technologies were chosen to provide solutions to the proposed problem and, finally, the way these technologies were used.

## Why Angular

Angular is a framework that provides robust tools to build Single Page Applications under a component-based architecture. Moreover, Angular provides other important tools such as unit test integration with Karma and Jasmine, or its own lintern. Thus, Angular, unlike other libraries like React or Vue, provides an all in one solution to build and deploy SPAs. 

In this specific case, I chose Angular because it gave me the following tools that helped me to build a solution to the proposed problem:
    - Fast code scaffolding provided by Angular CLI: with just one command(`ng new new-relic-app`), a whole deployable app can be created. Moreover, components, services or modules can be easily added by means of the command line.
    - Routing module to manage the navigation through the single page application components.
    - Intuitive, dynamic and efficient DOM access.
    - The integration of unit tests provided by the framework.

## Problem Solving

The proposed problem consisted on loading a list of applications provided in a json file, order them by their apdex index and classify them by the host they are deployed in. Moreover, a UI implementation following provided mock ups was also required. 

*  **Project structure:** as a first step, I designed a project structure designed according to the proposed problem. I decided to encapsulate my code in a child module (ApplicationsDashboardModule) containing all the components, models, services and other functionalities I needed. Thus, when the root module is executed, it loads the ApplicationsDashboardModule, which is in charge of the applications dashboard components, services and state management (with ngrx). This was done in order to provide scalability to the system, as in case more modules are required in the future (e.g a user authentication module), more child modules can be created from the root one and this structure would allow the system to take advantage of Angular modules lazy loading to optimize the application performance. Therefore, the project structure is the following:

    - app                                       //Root module
        - applications-dashboard                // Applications Dashboard Module
            - components
                - applications-dashboard        // Applications Dashboard Component
                - host                          // Host component
            - models                            
                - application.model.ts          // Application model
                - host.model.ts                 // Host model
            - services
                - data-api.service.ts           // Service that loads the data from the json file
            - store                             // ngrx state management functionalities
                - actions
                - effects
                - reducers
    - assets
        - fonts                                 // Imported font HelveticaNeueBold
        - scss

*  **Data collection:** the first step of this development was carrying out the data loading by means of the implementation of a Service (data-api.service.ts). The DataApiService includes a `get` function just like if the data was being provided by an external REST API, yet in this case the service points to the provided json file. 

The original data provided by the json file was loaded as an array of Application objects ordered by the javascript function `sort()`. Please note that the performance of this sorting could have been improved by means of the implementation of either quicksort algorithm, with an O(n2) worst case complexity. Finally this was not done due to time restrictions.

Afterwards, the applications were distributed by the host they were deployed in by means of a `Map<hostName:string, Application[]>` object. The use of a Map with an entry for each host provides a faster way to access a specific host data. 

*  **UI Building:** after the data is loaded, the ApplicationsDashboardComponent renders a child HostComponent for each entry of the map. This is carried out through the dynamic rendering provided by angular and, specifically through its `ngFor`directive. This way, each HostComponent receives a host name and an ordered list of Application objects as input parameters whereby the required information can be rendered through the `getTopFiveApps()` function and the data binding provided by Angular. Finally, the UI was built using html and scss, by means of flex-box display mostly.

*  **Optimization:** 

    *  **Map instead of array**: instead of using an Array of Host objects composed by a host name and an array of Application objects (approach 1), the chosen approach consisted of using a Map as data structure which provides a faster way to access each entry values list by its key (approach 2).

                Approach 1:

                    host: Host = {
                        name: string, 
                        applications: Application[]
                    }

                    hostList: Host [];

                Approach 2:

                    application: Application {
                        name: string;
                        contributors: string[];
                        version: number;
                        host: string[];
                        apdex: number;
                    }

                    hostsMap: Map<string, Application[]>;


    *  **NGRX and single source of truth:** for this specific problem, the data cohesion between all the displayed host components was very important because all of them consume from the same data source. Thus, if any change is triggered from one of the child components, the whole list should be notified and the data source must be updated. This is the reason why the "single source of truth" term came to my mind and I decided to use the library `ngrx`. This library provides a reactive state management inspired by Redux designed to improve performance and data consistency on top of Angular. 
    
    As it can be read in the project structure described above, a store folder was created inside the ApplicationsDashboardModule and three folders were added inside: actions, effects and reducers.

        - Reducer is where the state changes are managed and, in this case, the state will be the `host: Map<string, Application[]>` object to which all the host components are subscribed with a "Observer pattern".  
        - Actions are triggered by components in order to carry out a state change. Depending on the expected behaviour, actions can be captured either by the effect or the reducer. In this case, when the ApplicationsDashboardComponent is initialized, the action LoadApplications is triggered and captured by the effect, who gets the data from the DataApiService.
        - Effects are used to capture actions that require external interactions such as network requests. In this case, our effect communicates with the DataApiService and, after collecting the data, triggers a new Action that will be handled in the Reducer to carry out the state change. 

    This way, whenever the state changes in the reducer, every subscriber automatically receives this state update.


*  **Unit Testing:** Angular provides a unit test configuration that allows the creation of a TestBedModule that configures and initializes the desired environment for unit tests supported by Karma and Jasmine. This way, whenever a component is created through Angular CLI, it comes along with a component-name.spect.ts, where all the component tests can be implemented.

There are three test files in this project: app.component.spec.test for the root component, applications-dashboard.component.test for the ApplicationsDashboardComponent, and the host.component.spec.ts for the HostComponent.

## Deploying app in localhost

In order to deploy this app in local, the reader should follow the following steps:
        1. Clone the repository in a local folder.
        2. Execute `npm install` in the root route of the project in order to download all the dependencies inside the node_modules folder.
        3. Run de development server with `ng serve` and navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
